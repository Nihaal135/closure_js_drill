// function limitFunctionCallCount(cb, n) {
//     // Should return a function that invokes `cb`.
//     // The returned function should only allow `cb` to be invoked `n` times.
//     // Returning null is acceptable if cb can't be returned
// }
let limitFnc=require('../limitFunctionCallCount');
function callBack(){
    console.log("Hello !");
}
const limitObject=limitFnc(callBack,2);
limitObject();
limitObject();
limitObject();