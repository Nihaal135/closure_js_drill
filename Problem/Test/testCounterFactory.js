// function counterFactory() {
//     // Return an object that has two methods called `increment` and `decrement`.
//     // `increment` should increment a counter variable in closure scope and return it.
//     // `decrement` should decrement the counter variable and return it.
// }
let counterFnc=require('../counterFactory');
let counterObject=counterFnc();
console.log('Value after incrementing is '+ counterObject.increment());
console.log('Value after decrementing is '+ counterObject.decrement());