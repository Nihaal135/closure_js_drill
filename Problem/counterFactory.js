// function counterFactory() {
//     // Return an object that has two methods called `increment` and `decrement`.
//     // `increment` should increment a counter variable in closure scope and return it.
//     // `decrement` should decrement the counter variable and return it.
// }
function counterFactory(initialValue=10){
    let currentValue=initialValue;
    const functions={
        increment:function(incrementvalue=1){
            currentValue+=incrementvalue;
            return currentValue;
        },
        decrement:function(decrementvalue=1){
            currentValue-=decrementvalue;
            return currentValue;
        }
    };
    return functions;
}
module.exports=counterFactory;