// function limitFunctionCallCount(cb, n) {
//     // Should return a function that invokes `cb`.
//     // The returned function should only allow `cb` to be invoked `n` times.
//     // Returning null is acceptable if cb can't be returned
// }
function limitFunctionCallCount(callBackFnc,number=1){
    if(!callBackFnc || !number){
        return null;
    }
    function limitCaller(){
        if(number){
            number--;
            callBackFnc();
        }else{
            console.log('Number of times to invoke is exceeded.');
            return null;
        }
    };
    return limitCaller;
}
module.exports=limitFunctionCallCount;