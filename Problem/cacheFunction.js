// function cacheFunction(cb) {
//     // Should return a function that invokes `cb`.
//     // A cache (object) should be kept in closure scope.
//     // The cache should keep track of all arguments have been used to invoke this function.
//     // If the returned function is invoked with arguments that it has already seen
//     // then it should return the cached result and not invoke `cb` again.
//     // `cb` should only ever be invoked once for a given set of arguments.
// }
function cacheFunction(cb){
    let cache={};
    function invokeCache(...args){
        let argToString=JSON.stringify(args);
        if(cache.hasOwnProperty(argToString)){
            return cache[argToString];
        }
        else{
            let newArg=cb(...args);
            cache[argToString]=newArg;
            return newArg;
        }
    }
    return invokeCache;
}
module.exports=cacheFunction;